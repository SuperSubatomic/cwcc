# imports
from ics import Calendar, Event;
import requests;
import pandas as pd;
import string;
from datetime import datetime;

# Get the data and put it in a dataframe
response=requests.get("https://www.data.act.gov.au/api/views/jzzy-44un/rows.csv?accessType=DOWNLOAD");
content=str(response.content,"utf-8");

df=pd.DataFrame([x.split(",") for x in content.split("\n")]);
df.columns=df.iloc[0];
print(df);
df=df.sort_values(by="SUBURB");

# Write html header

stub="https://gitlab.com/SuperSubatomic/cwcc/-/jobs/artifacts/main/raw/public/calendars/"
endstub="?job=pages"

tz=10
duration=3;
now=datetime.now();
nowstr=(now+pd.DateOffset(hours=tz)).strftime("%Y/%m/%d %H:%M");

web=open("index.html","w");
header=open("header.html","r");
web.write(header.read());
header.close();
web.write("<p>Last updated "+nowstr+".</p>");

web.write("<table>");
# Now write each line out to a calendar (.ics format)

for index, row in df.iterrows() :
    if index>0 and row[2]!=None :
        c=Calendar();
        print(row);
        #if row["SUBURB"]=="DOWNER" :
        #   print(row);
        locstr=string.capwords(row["SUBURB"]+" "+row["SPLIT_SUBURB"]);
        for d in [["RECYCLING_PICKUP_DATE","Recycling"],["NEXT_GREENWASTE_DATE","Greenwaste"],["GARBAGE_PICKUP_DATE","Garbage"]] :
            date=datetime.strptime(row[d[0]],"%d/%m/%Y");
            eoff=Event();
            eoff.name=d[1]+" collection tomorrow ("+locstr+")";# updated "+nowstr+")";
            eoff.begin=(date+pd.DateOffset(hours=-24+18-tz)).strftime("%Y-%m-%d %H:%M:%S");
            eoff.end=(date+pd.DateOffset(hours=-24+18-tz+duration)).strftime("%Y-%m-%d %H:%M:%S");
            eon=Event();
            eon.name=d[1]+" collection today ("+locstr+")";# "+nowstr+")";
            eon.begin=(date+pd.DateOffset(hours=5-tz)).strftime("%Y-%m-%d %H:%M:%S");
            eon.end=(date+pd.DateOffset(hours=5-tz+duration)).strftime("%Y-%m-%d %H:%M:%S");
            c.events.add(eon);
            c.events.add(eoff);

        name=locstr.replace(" ","_").replace("'","");
        filename=name+".ics";
        file=open("calendars/"+filename,"w");
        file.writelines(c);
        file.close();

        file=open("calendars/"+filename,"r");
        lines=file.readlines();
        file.close();

        file=open("calendars/"+filename,"w");
        for line in lines :
            line=line.strip();
            file.write(line+"\r\n");
            if line=="BEGIN:VEVENT" :
                file.write("DTSTAMP:"+datetime.strftime(now,"%Y%m%dT%H%M%SZ")+"\r\n");
        file.close();
                
        # Now write html
        web.write("<tr>\n");
        web.write("<td><strong>"+locstr+"</strong>&nbsp;&nbsp;</td>");
        #web.write("<td><a href=\'https://calendar.google.com/calendar/render?cid="+stub.replace("https","webcal")+filename+endstub+"\'>Google</a></td>\n");
        web.write("<td><a href=\'"+stub+filename+endstub+"\'>Calendar link</a></td>\n");
        web.write("<td><a href=\'"+stub.replace("https","webcal")+filename+endstub+"\'>Apple devices</a></td>\n");
        web.write("</tr>\n");

web.write("</table>");
footer=open("footer.html","r");
web.write(footer.read());
footer.close();
