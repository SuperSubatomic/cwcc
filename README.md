# Canberra Waste Collection Calendars

Canberra Waste Collection Calendars in ics format. Original data from the ACT Government on behalf of ACT NOWaste.

Links to the calendars can be found [here](https://supersubatomic.gitlab.io/cwcc/).

Calendars provided with absolutely no warranty whatsoever and are not endorsed by the ACT Government in any way.
